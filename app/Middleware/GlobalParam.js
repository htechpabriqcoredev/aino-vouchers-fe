'use strict'

const Logger = use('Logger')
const Database = use('Database')
const ApiService = require('../Controllers/Http/Helper/ApiService.js')
const Env = use('Env')

class GlobalParam {
	async handle({ view, session }, next) {
		let settings = await Database.select('value').from('settings')
		// THEME SETTING
		let theme = null
		try{
			theme = session.get('theme')
			let state = null
			if(theme){
				state = view._locals.request.params.state
				if(state){
					if(theme.principal != state) {
						session.forget('theme')
						const apitheme = await ApiService.getData(Env.get('API_HOST') + '/theme/' + state)
						if(apitheme.code == '2000' && apitheme.data){
							theme = apitheme.data
							theme.principal = state
							theme.logo = Env.get('IMAGE_HOST') + theme.logo
							theme.banner = Env.get('IMAGE_HOST') + theme.banner
							session.put('theme', theme)
						} else {
							theme = null
						}
					}
				}
			} else{
				state = view._locals.request.params.state
				if(state){
					const apitheme = await ApiService.getData(Env.get('API_HOST') + '/theme/' + state)
					if(apitheme.code == '2000' && apitheme.data){
						theme = apitheme.data
						theme.principal = state
						theme.logo = Env.get('IMAGE_HOST') + theme.logo
						theme.banner = Env.get('IMAGE_HOST') + theme.banner
						session.put('theme', theme)
					}
				}
			}
		} catch(e){
			theme = null
		}
		// END THEME SETTING
		view.share({
			globalParam: settings,
			theme: theme
		})
		await next()
	}
}

module.exports = GlobalParam
'use strict'

const Model = use('Model')
const Hash = use('Hash')

class User extends Model {
	static boot() {
		super.boot()

		/**
		 * Hashing password before storing to the
		 * database.
		*/
		this.addHook('beforeCreate', 'User.hashPassword')
	}

	static get hidden() {
		return ['password']
	}

	static get rules() { 
		return {
			username: 'required',
			email: 'required|email'
		}
	}

	/**
	 * A relationship on tokens is required for auth to
	 * work. Since features like `refreshTokens` or
	 * `rememberToken` will be saved inside the
	 * tokens table.
	 *
	 * @method tokens
	 *
	 * @return {Object}
	*/
	tokens() {
		return this.hasMany('App/Models/Token')
	}

	/*
	|--------------------------------------------------------------------------
	| Relationship Methods
	|--------------------------------------------------------------------------
	*/
	/**
	 * Many-To-Many Relationship Method for accessing the User.roles
	 *
	 * @return Object
	 */
	roles() {
		return this.belongsToMany('App/Models/Role')
	}
}

module.exports = User
'use strict'

const minifyHTML = require('./Helper/MinifyHTML.js')
const ApiService = require('./Helper/ApiService.js')
const MyHash = require('./Helper/Hash.js')
const Helpers = use('Helpers')
const Strings = require('./Helper/String.js')
const Logger = use('Logger')
const Env = use('Env')
const customCookie = require('cookie')
const ms = require('ms')
const { validate, validateAll } = use('Validator')

class UserController {
	async getMyCard({ request, response, view, session }) {
		let user = session.get('user')
		let principal = session.get('principal')
		const { code, fr } = request.only(['code', 'fr'])

		if(user) {
			const options = {
				headers: {
					Authorization: 'Bearer ' + user.token.token
				}
			}

			let settings = await ApiService.getData(Env.get('API_HOST') + '/getsetting')
			let cards = await ApiService.getData(Env.get('API_HOST') + '/getcardlist/' + user['user']['id'], 'GET', null, options)
			let transactions = await ApiService.getData(Env.get('API_HOST') + '/gettransactionlistlimit/' + user['user']['id'], 'GET', null, options)
			let userpoint = await ApiService.getData(Env.get('API_HOST') + '/getuserpoint/' + user['user']['id'], 'GET', null, options)
			let meta = {
				title: 'My Card',
				description: 'My Card - ' + settings['data'][1]['value'],
				keywords: settings['data'][2]['value'],
				copyright: settings['data'][3]['value'],
				author: settings['data'][4]['value'],
				url: Env.get('BASE_URL'),
				image: Env.get('BASE_URL') + '/' + settings['data'][5]['value'],
				shortcutIcon: Env.get('IMAGE_HOST') + '/' + settings['data'][9]['value'],
				bigLogo: Env.get('IMAGE_HOST') + '/' + settings['data'][10]['value'],
				smallLogo: Env.get('IMAGE_HOST') + '/' + settings['data'][11]['value']
			}
			
			let template = view.render('my-card', {
				meta: meta,
				paramsFr: fr,
				params: principal ? principal : code,
				data: {
					mod: 'my-card',
					user: user,
					card: cards['data'] ? cards['data'] : null,
					transaction: transactions['data'] ? transactions['data'] : null,
					date: new Date(),
					userpoint: userpoint ? userpoint['data'] : 0,
				}
			})
			
			return await minifyHTML.minify(template)
		} else {
			return response.redirect('/login')
		}
	}

	async getCards({ request, response, view, session }) {
		let user = session.get('user')
		let principal = session.get('principal')
		let principal_id = session.get('principal_id')

		if(user) {
			const options = {
				headers: {
					Authorization: 'Bearer ' + user.token.token
				}
			}

			let settings = await ApiService.getData(Env.get('API_HOST') + '/getsetting')
			let cards = await ApiService.getData(Env.get('API_HOST') + '/getissuerprincipal/' + principal_id, 'GET', null, options)
			let meta = {
				title: 'Add Card',
				description: 'Add Card - ' + settings['data'][1]['value'],
				keywords: settings['data'][2]['value'],
				copyright: settings['data'][3]['value'],
				author: settings['data'][4]['value'],
				url: Env.get('BASE_URL'),
				image: Env.get('BASE_URL') + '/' + settings['data'][5]['value'],
				shortcutIcon: Env.get('IMAGE_HOST') + '/' + settings['data'][9]['value'],
				bigLogo: Env.get('IMAGE_HOST') + '/' + settings['data'][10]['value'],
				smallLogo: Env.get('IMAGE_HOST') + '/' + settings['data'][11]['value']
			}
			
			let template = view.render('cards', {
				meta: meta,
				params: principal,
				data: {
					mod: 'cards',
					user: user,
					cards: cards['data']['data']
				}
			})
			
			return await minifyHTML.minify(template)
		} else {
			return response.redirect('/home/'+principal)
		}
	}

	async getAddCard({ request, response, view, session }) {
		let user = session.get('user')
		let principal = session.get('principal')
		let issuer = request.only(['is'])

		if(user) {
			const options = {
				headers: {
					Authorization: 'Bearer ' + user.token.token
				}
			}

			let settings = await ApiService.getData(Env.get('API_HOST') + '/getsetting')
		// 	
			let meta = {
				title: 'Add Card',
				description: 'Add Card - ' + settings['data'][1]['value'],
				keywords: settings['data'][2]['value'],
				copyright: settings['data'][3]['value'],
				author: settings['data'][4]['value'],
				url: Env.get('BASE_URL'),
				image: Env.get('BASE_URL') + '/' + settings['data'][5]['value'],
				shortcutIcon: Env.get('IMAGE_HOST') + '/' + settings['data'][9]['value'],
				bigLogo: Env.get('IMAGE_HOST') + '/' + settings['data'][10]['value'],
				smallLogo: Env.get('IMAGE_HOST') + '/' + settings['data'][11]['value']
			}
			
			let template = view.render('add-card', {
				meta: meta,
				params: principal,
				data: {
					mod: 'add-card',
					user: user,
					issuer: issuer.is
				}
			})
			
			return await minifyHTML.minify(template)
		} else {
			return response.redirect('/home/'+principal)
		}
	}

	async postAddCard({ request, response, view, session }) {
		const { card_number, issuer } = request.only(['card_number', 'issuer'])
		let user = session.get('user')
		let principal = session.get('principal')
		let principal_id = session.get('principal_id')

		const reqparam = {
			card_number: card_number,
			user_id: user['user']['id'],
			issuer_id: issuer,
			principal_code: principal,
			principal_id: principal_id
		}
		
		const rules = {
			card_number: 'required|min:15|max:17',
			user_id: 'required',
			issuer_id: 'required',
			principal_code: 'required',
			principal_id: 'required'
		}

		const messages = {
			'card_number.min': 'Your Card Number less than 15 character',
			'card_number.max': 'Your Card Number more than 17 character',
		}
		
		const validation = await validateAll(reqparam, rules)
		
		if (validation.fails()) {
			session.withErrors(validation.messages()).flashAll()
			return response.redirect('back')
		} else {
			let addcard = await ApiService.getData(Env.get('API_HOST') + '/postaddcard', 'POST', reqparam)
			if (addcard['code'] == '2000') {
				session.flash({ notification: 'New Card Added' })
				return 1
			} else if(addcard['code'] == '4005') {
				session.flash({ notification: 'Card already registered with same card!' })
				return 2
			} else if(addcard['code'] == '4002') {
				session.flash({ notification: 'Card already registered by other!' })
				return 3
			} else {
				session.flash({ notification: 'Oh Snap! Something happen' })
				return 4
			}
		}
	}

	async getMyAccount({ params, request, response, view, session }) {
		let user = session.get('user')
		let principal = session.get('principal')

		if(user) {
			const options = {
				headers: {
					Authorization: 'Bearer ' + user.token.token
				}
			}

			let settings = await ApiService.getData(Env.get('API_HOST') + '/getsetting')
			let customer = await ApiService.getData(Env.get('API_HOST') + '/getcustomerdetail/' + user['user']['id'], 'GET', null, options)
			let newuser = await ApiService.getData(Env.get('API_HOST') + '/getuserdetail/' + user['user']['id'], 'GET', null, options)
			
			let meta = {
				title: 'My Account',
				description: 'My Account - ' + settings['data'][1]['value'],
				keywords: settings['data'][2]['value'],
				copyright: settings['data'][3]['value'],
				author: settings['data'][4]['value'],
				url: Env.get('BASE_URL'),
				image: Env.get('BASE_URL') + '/' + settings['data'][5]['value'],
				shortcutIcon: Env.get('IMAGE_HOST') + '/' + settings['data'][9]['value'],
				bigLogo: Env.get('IMAGE_HOST') + '/' + settings['data'][10]['value'],
				smallLogo: Env.get('IMAGE_HOST') + '/' + settings['data'][11]['value']
			}
			
			let template = view.render('my-account', {
				meta: meta,
				params: principal,
				data: {
					// mod: 'account-edit',
					user: user,
					customer: customer['data'],
					newuser: newuser['data']
				}
			})
			
			return await minifyHTML.minify(template)
		} else {
			return response.redirect('/home/' + principal)
		}
	}

	async getMyAccountEdit({ request, response, view, session }) {
		let user = session.get('user')
		let principal = session.get('principal')

		if(user) {
			const options = {
				headers: {
					Authorization: 'Bearer ' + user.token.token
				}
			}

			let settings = await ApiService.getData(Env.get('API_HOST') + '/getsetting')
			let customer = await ApiService.getData(Env.get('API_HOST') + '/getcustomerdetail/' + user['user']['id'], 'GET', null, options)
			let newuser = await ApiService.getData(Env.get('API_HOST') + '/getuserdetail/' + user['user']['id'], 'GET', null, options)
			
			let meta = {
				title: 'Edit Account',
				description: 'Edit Account - ' + settings['data'][1]['value'],
				keywords: settings['data'][2]['value'],
				copyright: settings['data'][3]['value'],
				author: settings['data'][4]['value'],
				url: Env.get('BASE_URL'),
				image: Env.get('BASE_URL') + '/' + settings['data'][5]['value'],
				shortcutIcon: Env.get('IMAGE_HOST') + '/' + settings['data'][9]['value'],
				bigLogo: Env.get('IMAGE_HOST') + '/' + settings['data'][10]['value'],
				smallLogo: Env.get('IMAGE_HOST') + '/' + settings['data'][11]['value']
			}
			
			let template = view.render('my-account-edit', {
				meta: meta,
				params: principal,
				data: {
					// mod: 'account-edit',
					user: user,
					customer: customer['data'],
					newuser: newuser['data']
				}
			})
			
			return await minifyHTML.minify(template)
		} else {
			return response.redirect('/home/' + principal)
		}
	}

	async postMyAccountEdit({ request, response, view, session }) {
		let user = session.get('user')

		const { fullname, gender, telephone, email, date_of_birth, identity_number, address, picture_name } = request.only(['fullname', 'gender', 'telephone', 'email', 'date_of_birth', 'identity_number', 'address', 'picture_name'])

		const picture = request.file('picture', {
			types: ['image'],
			size: '2gb'
		})

		let namePicture
		let namePics

		if(picture != null) {
			const type = request.file('picture').subtype;
			namePicture = `${new Date().getTime()}_picture_${user.user.id}.${type}`;
			
			await picture.move(Helpers.publicPath('/uploads/images/'), {
				name: namePicture
			})

			if (!picture.moved()) {
				return picture.error()
			}
		} else {
			if(picture_name == null || picture_name == 'null') {
				namePics = '/theme/images/icon/home-03.png'
			} else {
				namePics = picture_name
			}
		}
		
		
		const reqparam = {
			fullname: fullname,
			gender: gender,
			telephone: telephone,
			email: email,
			date_of_birth: date_of_birth,
			identity_number: identity_number,
			address: address,
			image: picture ? '/uploads/images/' + namePicture : namePics,
			user_id: user.user.id,
		}

		const options = {
			headers: {
				Authorization: 'Bearer ' + user.token.token
			}
		}

		const rules = {
			fullname: 'required|min:3|max:20',
			date_of_birth: 'required',
			identity_number: 'min:16|max:16',
			address: 'max:50'
		}

		const messages = {
			'fullname.required': 'Enter your fullname',
			'fullname.min': 'Your fullname cannot less than 3 character',
			'fullname.max': 'Your fullname cannot more than 20 character',
			'date_of_birth.required': 'Enter your date of birth',
			'identity_number.min': 'Your identity number must 16 character',
			'identity_number.max': 'Your identity number must 16 character',
			'address.max': 'Your address cannot more than 50 character',
		}
		
		const validation = await validateAll(reqparam, rules, messages)
		
		if (validation.fails()) {
			session.withErrors(validation.messages())
			return response.redirect('back')
		} else {
			let customeredit = await ApiService.getData(Env.get('API_HOST') + '/postmyaccount', 'POST', reqparam, options)
			
			if (customeredit['code'] == '2000') {
				session.flash({ notification: 'Update Account Success!' })
				return response.redirect('/my-account')
			} else {
				session.flash({ notification: 'Check your data first!' })
				return response.redirect('back')
			}
		}
	}

	async getChangePassword({ request, response, view, session }) {
		let user = session.get('user')
		let principal = session.get('principal')

		if(user) {
			const options = {
				headers: {
					Authorization: 'Bearer ' + user.token.token
				}
			}

			let settings = await ApiService.getData(Env.get('API_HOST') + '/getsetting?id=1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22')
		
			let meta = {
				title: 'Change Password',
				description: 'Change Password - ' + settings['data'][1]['value'],
				keywords: settings['data'][2]['value'],
				copyright: settings['data'][3]['value'],
				author: settings['data'][4]['value'],
				url: Env.get('BASE_URL'),
				image: Env.get('BASE_URL') + '/' + settings['data'][5]['value'],
				shortcutIcon: Env.get('IMAGE_HOST') + '/' + settings['data'][9]['value'],
				bigLogo: Env.get('IMAGE_HOST') + '/' + settings['data'][10]['value'],
				smallLogo: Env.get('IMAGE_HOST') + '/' + settings['data'][11]['value']
			}
			
			let template = view.render('change-password', {
				meta: meta,
				params: principal,
				data: {
					// mod: 'account-edit',
					user: user,
				}
			})
			
			return await minifyHTML.minify(template)
		} else {
			return response.redirect('/home/' + principal)
		}
	}

	async postChangePassword({ request, response, view, session }) {
		let user = session.get('user')

		const { old_password, new_password, confirm_password } = request.only(['old_password', 'new_password', 'confirm_password'])
		
		const reqparam = {
			old_password: old_password,
			new_password: new_password,
			confirm_password: confirm_password,
			user_id: user.user.id,
		}

		const options = {
			headers: {
				Authorization: 'Bearer ' + user.token.token
			}
		}

		const rules = {
			old_password: 'required',
			new_password: 'required|min:6',
			confirm_password: 'required|min:6',
		}

		const messages = {
			'old_password.required': 'Enter your old password',
			'new_password.required': 'Enter your new password',
			'confirm_password.required': 'Re-enter the password',
			'new_password.min': 'Your password must have at least 6 characters with 1 alphabet and 1 number ',
			'confirm_password.min': 'Your password must have at least 6 characters with 1 alphabet and 1 number ',
		}
		
		const validation = await validateAll(reqparam, rules, messages)
		
		if (validation.fails()) {
			session.withErrors(validation.messages())
			return response.redirect('back')
		} else {
			if (new_password == confirm_password) {
				let editpassword = await ApiService.getData(Env.get('API_HOST') + '/postpassword', 'POST', reqparam, options)
				
				if (editpassword['code'] == '2000') {
					session.flash({ notification: 'You’ve got yourself a new password' })
					return response.redirect('/my-account')
				} else if(editpassword['code'] == '4004') {
					session.flash({ notification: 'Wrong Password!' })
					return response.redirect('/change-password')
				} else {
					session.flash({ notification: 'Check your password!' })
					return response.redirect('/change-password')
				}
			} else {
				session.flash({ notification: 'New password not match with confirm password' })
				return response.redirect('/change-password')
			}
		}
	}

	async getTermsConditions({ request, response, view, session }) {
		let user = session.get('user')
		let principal = session.get('principal')

		if(user) {
			const options = {
				headers: {
					Authorization: 'Bearer ' + user.token.token
				}
			}

			let settings = await ApiService.getData(Env.get('API_HOST') + '/getsetting')
		
			let meta = {
				title: 'Terms & Conditions',
				description: 'Terms & Conditions - ' + settings['data'][1]['value'],
				keywords: settings['data'][2]['value'],
				copyright: settings['data'][3]['value'],
				author: settings['data'][4]['value'],
				url: Env.get('BASE_URL'),
				image: Env.get('BASE_URL') + '/' + settings['data'][5]['value'],
				shortcutIcon: Env.get('IMAGE_HOST') + '/' + settings['data'][9]['value'],
				bigLogo: Env.get('IMAGE_HOST') + '/' + settings['data'][10]['value'],
				smallLogo: Env.get('IMAGE_HOST') + '/' + settings['data'][11]['value']
			}
			
			let template = view.render('terms-conditions', {
				meta: meta,
				params: principal,
				data: {
					mod: 'terms-conditions',
					user: user,
				}
			})
			
			return await minifyHTML.minify(template)
		} else {
			return response.redirect('/home/'+principal)
		}
	}

	async getFaq({ request, response, view, session }) {
		let user = session.get('user')
		let principal = session.get('principal')

		if(user) {
			const options = {
				headers: {
					Authorization: 'Bearer ' + user.token.token
				}
			}

			let settings = await ApiService.getData(Env.get('API_HOST') + '/getsetting')
		
			let meta = {
				title: 'Terms & Conditions',
				description: 'Terms & Conditions - ' + settings['data'][1]['value'],
				keywords: settings['data'][2]['value'],
				copyright: settings['data'][3]['value'],
				author: settings['data'][4]['value'],
				url: Env.get('BASE_URL'),
				image: Env.get('BASE_URL') + '/' + settings['data'][5]['value'],
				shortcutIcon: Env.get('IMAGE_HOST') + '/' + settings['data'][9]['value'],
				bigLogo: Env.get('IMAGE_HOST') + '/' + settings['data'][10]['value'],
				smallLogo: Env.get('IMAGE_HOST') + '/' + settings['data'][11]['value']
			}
			
			let template = view.render('faq', {
				meta: meta,
				params: principal,
				data: {
					mod: 'faq',
					user: user,
				}
			})
			
			return await minifyHTML.minify(template)
		} else {
			return response.redirect('/home/'+principal)
		}
	}

	async getTransactionHistory({ request, response, view, session }) {
		let user = session.get('user')
		let principal = session.get('principal')

		if(user) {
			const options = {
				headers: {
					Authorization: 'Bearer ' + user.token.token
				}
			}

			let settings = await ApiService.getData(Env.get('API_HOST') + '/getsetting')
		
			let meta = {
				title: 'Transaction History',
				description: 'Transaction History - ' + settings['data'][1]['value'],
				keywords: settings['data'][2]['value'],
				copyright: settings['data'][3]['value'],
				author: settings['data'][4]['value'],
				url: Env.get('BASE_URL'),
				image: Env.get('BASE_URL') + '/' + settings['data'][5]['value'],
				shortcutIcon: Env.get('IMAGE_HOST') + '/' + settings['data'][9]['value'],
				bigLogo: Env.get('IMAGE_HOST') + '/' + settings['data'][10]['value'],
				smallLogo: Env.get('IMAGE_HOST') + '/' + settings['data'][11]['value']
			}
			
			let template = view.render('transaction-history', {
				meta: meta,
				params: principal,
				data: {}
			})
			
			return await minifyHTML.minify(template)
		} else {
			return response.redirect('/home/'+principal)
		}
	}

	async postTransactionData({request, response, view, session}) {
		let user = session.get('user')
		let principal = session.get('principal')
		let req = request.all()

		let reqparam = {
			user_id: user['user']['id'],
			limit: req.limit,
			offset: req.offset
		}

		let transactions = await ApiService.getData(Env.get('API_HOST') + '/posttransactionlist', 'POST', reqparam)
		
		return transactions['data']
	}

	async postAgreeTnc({request, response, view, session}) {
		let user = session.get('user')
		let principal = session.get('principal')
		let req = request.all()

		let reqparam = {
			user_id: user['user']['id'],
			checked: req.checked
		}

		let agree = await ApiService.getData(Env.get('API_HOST') + '/postagreetnc', 'POST', reqparam)
		
		return agree
	}

	async postDeleteCard({request, response, view, session}) {
		let user = session.get('user')
		let principal = session.get('principal')
		let req = request.all()

		let reqparam = {
			id: req.card_id
		}

		let cards = await ApiService.getData(Env.get('API_HOST') + '/postdeletecard', 'POST', reqparam)

		if(cards['code'] == '2000') {
			session.flash({ notification: 'Delete Card Success' })
			return 1
		} else {
			session.flash({ notification: 'Delete Card Failed' })
			return 2
		}
		
		return cards
	}
}

module.exports = UserController
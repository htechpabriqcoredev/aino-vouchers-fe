'use strict'

const ApiService = use('App/Models/ApiService')
const Setting = use('App/Models/Setting')
const { validate, validateAll } = use('Validator')
const Hash = use('Hash')
const Database = use('Database')
const QueryBuilder = require('./Helper/DatatableBuilder.js')
const CheckAuth = require('./Helper/CheckAuth.js')
const codeGen = require('./Helper/CodeGenerator.js')
const minifyHTML = require('./Helper/MinifyHTML.js')
const Env = use('Env')
const Logger = use('Logger')

class ApiServiceController {
	async index({request, response, auth, view}) {
		let checkAuth = await CheckAuth.get('ApiService', 'read', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}
		
		let settings = (await Setting.query().fetch()).toJSON()
		
		let meta = {
			title: settings[0]['value'],
			description: settings[1]['value'],
			keywords: settings[2]['value'],
			copyright: settings[3]['value'],
			author: settings[4]['value'],
			shortcutIcon: Env.get('IMAGE_HOST') + '/' + settings[9]['value'],
			bigLogo: Env.get('IMAGE_HOST') + '/' + settings[10]['value'],
			smallLogo: Env.get('IMAGE_HOST') + '/' + settings[11]['value']
			url: Env.get('BASE_URL'),
			image: Env.get('BASE_URL') + '/' + settings[20]['value']
		}
		
		let template = view.render('apiservice.index', {
			meta: meta
		})
		
		return await minifyHTML.minify(template)
	}
	
	async datatable({request, response, auth, view, session}) {
		const formData = request.post()
		
		let tableDefinition = {
			sTableName: 'api_services',
			sSelectSql: ['id', 'client_name', 'client_key', 'client_secret', 'permissions', 'status'],
			aSearchColumns: ['client_name', 'client_key', 'client_secret', 'permissions', 'status']
		}
		
		let queryBuilder = new QueryBuilder(tableDefinition)
		
		let requestQuery = {
			draw: formData.draw,
			columns: formData.columns,
			order: formData.order,
			start: formData.start,
			length: formData.length,
			search: formData.search
		}
		
		let queries = queryBuilder.buildQuery(JSON.parse(JSON.stringify(requestQuery)))
		
		let select = await Database.raw(queries.select)
		let recordsTotal = await Database.raw(queries.recordsTotal)
		let recordsFiltered = await Database.raw(queries.recordsFiltered)
		
		let fdata = []
		let no = 0
		for(let x in select[0]) {
			fdata.push([
				"<div class='text-center'><input type='checkbox' id='titleCheckdel' /><input type='hidden' class='deldata' name='item[]' value='"+ select[0][x]['id'] +"' disabled /></div>\n",
				select[0][x]['id'],
				select[0][x]['client_name'],
				select[0][x]['client_key'],
				select[0][x]['status'] == '1' ? 'Active' : 'Non Active',
				"<div class='text-center'><div class='btn-group btn-group-sm'><a href='./apiservice/"+ select[0][x]['id'] +"/edit' class='btn btn-sm btn-primary' id='"+ select[0][x]['id'] +"' data-toggle='tooltip' title='Edit'><i class='fa fa-pencil'></i></a><a class='btn btn-sm btn-danger alertdel' id='"+ select[0][x]['id'] +"' data-toggle='tooltip' title='Delete'><i class='fa fa-times'></i></a></div></div>\n"
			])
			no++
		}
		
		let data = {
			draw: formData.draw,
			recordsTotal: JSON.stringify(recordsTotal[0][0]['COUNT(*)']),
			recordsFiltered: (queries.recordsFiltered) ? JSON.stringify(recordsFiltered[0][0]['COUNT(*)']) : JSON.stringify(recordsTotal[0][0]['COUNT(*)']),
			data: fdata
		}
		
		return data
	}
	
	async create({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('ApiService', 'create', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}
		
		let settings = (await Setting.query().fetch()).toJSON()
		
		let meta = {
			title: settings[0]['value'],
			description: settings[1]['value'],
			keywords: settings[2]['value'],
			copyright: settings[3]['value'],
			author: settings[4]['value'],
			url: Env.get('BASE_URL'),
			image: Env.get('BASE_URL') + '/' + settings[20]['value'],
			shortcutIcon: Env.get('IMAGE_HOST') + '/' + settings[9]['value'],
			bigLogo: Env.get('IMAGE_HOST') + '/' + settings[10]['value'],
			smallLogo: Env.get('IMAGE_HOST') + '/' + settings[11]['value']
		}
		
		let template = view.render('apiservice.create', {
			meta: meta
		})
		
		return await minifyHTML.minify(template)
	}
	
	async store({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('ApiService', 'create', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}
		
		const { client_name, client_key, client_secret, permissions, status } = request.only(['client_name', 'client_key', 'client_secret', 'permissions', 'status'])
		
		let generateKey = codeGen.generate({
			length: 32,
			count: 1,
			charset: codeGen.charset('alphacasenumeric')
		})
		
		let generateSecret = codeGen.generate({
			length: 64,
			count: 1,
			charset: codeGen.charset('alphacasenumeric')
		})
		
		let formData = {
			client_name: client_name,
			client_key: generateKey[0],
			client_secret: generateSecret[0],
			permissions: permissions,
			status: '1',
			created_by: auth.user.id,
			updated_by: auth.user.id
		}
		
		const rules = {
			client_name: 'required',
			client_key: 'required',
			client_secret: 'required'
		}
		
		const validation = await validateAll(formData, rules)
		if (validation.fails()) {
			session.withErrors(validation.messages())
			return response.redirect('back')
		} else {
			await ApiService.create(formData)
			session.flash({ notification: 'API service added', status: 'aquamarine' })
			return response.redirect('/apiservice')
		}
	}
	
	async edit({params, request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('ApiService', 'update', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}
		
		let apiservice = await ApiService.find(params.id)

		if (apiservice) {
			let settings = (await Setting.query().fetch()).toJSON()
		
			let meta = {
				title: settings[0]['value'],
				description: settings[1]['value'],
				keywords: settings[2]['value'],
				copyright: settings[3]['value'],
				author: settings[4]['value'],
				url: Env.get('BASE_URL'),
				image: Env.get('BASE_URL') + '/' + settings[20]['value'],
				shortcutIcon: Env.get('IMAGE_HOST') + '/' + settings[9]['value'],
				bigLogo: Env.get('IMAGE_HOST') + '/' + settings[10]['value'],
				smallLogo: Env.get('IMAGE_HOST') + '/' + settings[11]['value']
			}
			
			let template = view.render('apiservice.edit', {
				meta: meta,
				apiservice: apiservice
			})
			
			return await minifyHTML.minify(template)
		} else {
			session.flash({ notification: 'API service not found', status: 'danger' })
			return response.redirect('/apiservice')
		}
	}
	
	async update({params, request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('ApiService', 'update', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}
		
		const { client_name, client_key, client_secret, permissions, status, generate } = request.only(['client_name', 'client_key', 'client_secret', 'permissions', 'status', 'generate'])
		
		let apiservice = await ApiService.find(params.id)
		
		let generateSecret = codeGen.generate({
			length: 64,
			count: 1,
			charset: codeGen.charset('alphacasenumeric')
		})
		
		let formData
		if (generate || generate == '1') {
			formData = {
				client_name: client_name,
				client_secret: generateSecret[0],
				permissions: permissions,
				status: status,
				updated_by: auth.user.id
			}
		} else {
			formData = {
				client_name: client_name,
				permissions: permissions,
				status: status,
				updated_by: auth.user.id
			}
		}
		
		let rules = {
			client_name: 'required'
		}
		
		const validation = await validateAll(formData, rules)
		if (validation.fails()) {
			session.withErrors(validation.messages())
			return response.redirect('back')
		} else {
			if (apiservice) {
				await ApiService.query().where('id', params.id).update(formData)
				session.flash({ notification: 'API service updated', status: 'aquamarine' })
				return response.redirect('/apiservice')
			} else {
				session.flash({ notification: 'API service not found', status: 'danger' })
				return response.redirect('/apiservice')
			}
		}
	}
	
	async delete({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('ApiService', 'delete', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}
		
		const formData = request.all()
		let apiservice = await ApiService.find(formData.id)
		if (apiservice){
			try {
				await apiservice.delete()
				session.flash({ notification: 'API service success deleted', status: 'aquamarine' })
				return response.redirect('/apiservice')
			} catch (e) {
				session.flash({ notification: 'API service cannot be delete', status: 'danger' })
				return response.redirect('/apiservice')
			}
		} else {
			session.flash({ notification: 'API service cannot be delete', status: 'danger' })
			return response.redirect('/apiservice')
		}
	}
	
	async multidelete({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('ApiService', 'delete', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}
		
		const formData = request.all()
		if (formData.totaldata != '0') {
			for (let i in formData.item) {
				let apiservice = await ApiService.find(formData.item[i])
				try {
					await apiservice.delete()
				} catch (e) {}
			}
			session.flash({ notification: 'API service success deleted', status: 'aquamarine' })
			return response.redirect('/apiservice')
		} else {
			session.flash({ notification: 'API service cannot be deleted', status: 'danger' })
			return response.redirect('/apiservice')
		}
	}
}

module.exports = ApiServiceController
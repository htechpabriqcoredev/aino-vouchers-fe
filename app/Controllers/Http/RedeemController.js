'use strict'

const minifyHTML = require('./Helper/MinifyHTML.js')
const ApiService = require('./Helper/ApiService.js')
const MyHash = require('./Helper/Hash.js')
const Helpers = use('Helpers')
const Strings = require('./Helper/String.js')
const Logger = use('Logger')
const Env = use('Env')
const customCookie = require('cookie')
const ms = require('ms')
const moment = require('moment')
const { validate, validateAll } = use('Validator')

class RedeemController {
	async getRedeem({ request, response, view, session }) {
		let user = session.get('user')
		let principal = session.get('principal')
		const { id, fr } = request.only(['id', 'fr'])

		if(user) {
			const options = {
				headers: {
					Authorization: 'Bearer ' + user.token.token
				}
			}

			let reqparam = {
				reward_id: id
			}

			let settings = await ApiService.getData(Env.get('API_HOST') + '/getsetting')
			let rewarddetails = await ApiService.getData(Env.get('API_HOST') + '/postrewarddetails', 'POST', reqparam)
			let userpoint = await ApiService.getData(Env.get('API_HOST') + '/getuserpoint/' + user['user']['id'], 'GET', null)
			
			let meta = {
				title: 'Redeem',
				description: 'Redeem - ' + settings['data'][1]['value'],
				keywords: settings['data'][2]['value'],
				copyright: settings['data'][3]['value'],
				author: settings['data'][4]['value'],
				url: Env.get('BASE_URL'),
				image: Env.get('BASE_URL') + '/' + settings['data'][5]['value'],
				shortcutIcon: Env.get('IMAGE_HOST') + '/' + settings['data'][9]['value'],
				bigLogo: Env.get('IMAGE_HOST') + '/' + settings['data'][10]['value'],
				smallLogo: Env.get('IMAGE_HOST') + '/' + settings['data'][11]['value']
			}
			
			let template = view.render('redeem', {
				meta: meta,
				params: principal,
				paramsFr: fr,
				data: {
					mod: 'redeem',
					user: user,
					reward_id_encrypt: id,
					rewards: rewarddetails['data'],
					userpoint: userpoint['data']
				}
			})
			
			return await minifyHTML.minify(template)
		} else {
			return response.redirect('/login')
		}
	}

	async postCreateOrderRedeem({request, response, view, session}) {
		let req = request.all()

		let reqparam = {
			fullname: req.fullname,
			email: req.email,
			phone_number: req.phone_number,
			address: req.address,
			user_id: req.user_id,
			address: req.address ? req.address : '-',
			issuer_name: req.issuer_name,
			reward_id: req.reward_id,
			reward_used: req.reward_used,
			reward_name: req.reward_name,
			date: moment(new Date()).format('YYYY-MM-DD hh:mm:ss')
		}

		let redeems = await ApiService.getData(Env.get('API_HOST') + '/postcreateorderredeem', 'POST', reqparam)
		
		return redeems
	}

	async getRedeemCreditCard({ request, response, view, session }) {
		// let user = session.get('user')

		// if(user) {
		// 	const options = {
		// 		headers: {
		// 			Authorization: 'Bearer ' + user.token.token
		// 		}
		// 	}

		// 	let settings = await ApiService.getData(Env.get('API_HOST') + '/getsetting?id=1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22')
		// 	let customer = await ApiService.getData(Env.get('API_HOST') + '/getcustomerdetail/' + user['user']['id'], 'GET', null, options)
		// 	let customeraddress = await ApiService.getData(Env.get('API_HOST') + '/getcustomeraddress/' + user['user']['id'], 'GET', null, options)
		// 	let latesttrx = await ApiService.getData(Env.get('API_HOST') + '/getlatesttransaction/' + user['customer']['id'], 'GET', null, options)
		// 	let latesttrxwithdraw = await ApiService.getData(Env.get('API_HOST') + '/getlatesttransactionwithdraw/' + user['customer']['id'], 'GET', null, options)
			
			let meta = {
				title: 'Redeem',
				// description: 'Rangkuman Akun - ' + settings['data'][1]['value'],
				// keywords: settings['data'][2]['value'],
				// url: 'https://www.aino.com/login',
				// image: 'https://www.aino.com/' + settings['data'][20]['value']
			}
			
			let template = view.render('redeem-creditcard', {
				meta: meta,
				data: {
					mod: 'redeem-creditcard',
					// user: user,
					// customer: customer['data'],
					// customeraddress: customeraddress['data'],
					// latesttrx: latesttrx['data'],
					// totalbalance: latesttrxwithdraw['data']['wallets'],
					// latesttrxwithdraw: latesttrxwithdraw['data']['walletswithdraw']
				}
			})
			
			return await minifyHTML.minify(template)
		// } else {
		// 	return response.redirect('/')
		// }
	}
}

module.exports = RedeemController
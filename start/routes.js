'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {import('@adonisjs/framework/src/Route/Manager'} */
const Route = use('Route')

// Router for Frontend
Route.get('login', 'AccountController.getLogin').middleware('globalparam')
Route.post('postlogin', 'AccountController.postLogin')
Route.get('signup', 'AccountController.register').middleware('globalparam')
Route.post('postsignup', 'AccountController.postRegister')
Route.get('logout', 'AccountController.logout')
Route.get('forgot', 'AccountController.forgotPassword').middleware('globalparam')
Route.post('postforgot', 'AccountController.postForgot')
Route.get('reset/:id/:key', 'AccountController.reset').middleware('globalparam')
Route.get('activation/:id/:key', 'AccountController.activation').middleware('globalparam')
Route.post('postreset', 'AccountController.postReset')
Route.get('otp', 'AccountController.otpPages').middleware('globalparam')
Route.post('postotp', 'AccountController.postOtp')
Route.post('postresendotp', 'AccountController.postResendOtp')
Route.get('error', 'ErrorController.index')
Route.get('authenticate/:secretcode', 'AccountController.gettAuthenticateSSO')

// Router for Backend
Route.group(() => {
	// Route.get('/', 'HomeController.notFound')
	Route.get('home/:state', 'HomeController.getHome')

	Route.get('filemanager', 'FilemanagerController.index')
	Route.get('filemanager/getdata', 'FilemanagerController.getData').formats(['json'])
	Route.post('filemanager/getdata', 'FilemanagerController.getData').formats(['json'])
	Route.post('filemanager/upload', 'FilemanagerController.upload').formats(['json'])
	Route.post('filemanager/delete', 'FilemanagerController.delete').formats(['json'])

	Route.get('my-card', 'UserController.getMyCard')
	Route.get('cards', 'UserController.getCards')
	Route.get('add-card', 'UserController.getAddCard')
	Route.post('postaddcard', 'UserController.postAddCard')

	Route.get('my-account', 'UserController.getMyAccount')
	Route.get('my-account-edit', 'UserController.getMyAccountEdit')
	Route.post('postmyaccount', 'UserController.postMyAccountEdit')
	Route.get('change-password', 'UserController.getChangePassword')
	Route.post('postchangepassword', 'UserController.postChangePassword')
	Route.post('transactiondata', 'UserController.postTransactionData').formats(['json'])
	Route.post('agreetnc', 'UserController.postAgreeTnc').formats(['json'])
	Route.post('deletecard', 'UserController.postDeleteCard').formats(['json'])

	Route.get('notifications', 'NotificationController.getNotification')
	Route.post('postnotifications', 'NotificationController.postNotificationData').formats(['json'])

	Route.get('terms-conditions', 'UserController.getTermsConditions')
	Route.get('faq', 'UserController.getFaq')
	Route.get('transaction-history', 'UserController.getTransactionHistory')

	Route.get('reward-catalog', 'RewardController.getRewardCatalog')
	Route.get('my-rewards', 'RewardController.getMyReward')
	Route.get('my-reward-details', 'RewardController.getMyRewardDetail')
	Route.get('my-reward-details-use', 'RewardController.getUseRewardDetail')
	Route.get('reward-details', 'RewardController.getRewardDetail')
	Route.get('ourpicks', 'RewardController.getOurPicks')
	Route.get('bestseller', 'RewardController.getBestSeller')
	Route.post('bestsellerdata', 'RewardController.postBestSellerData').formats(['json'])
	Route.post('ourpicksdata', 'RewardController.postOurPicksData').formats(['json'])
	Route.get('transaction-history', 'RewardController.getTransactionHistory')
	Route.post('myrewarddata', 'RewardController.postMyRewardData').formats(['json'])
	Route.get('reward-categories', 'RewardController.getRewardCategories')
	Route.post('categoriesdata', 'RewardController.postCategoriesData').formats(['json'])
	Route.post('newdealsdata', 'RewardController.postCategoriesNewDealsData').formats(['json'])
	

	Route.get('redeem', 'RedeemController.getRedeem')
	Route.get('redeem-creditcard', 'RedeemController.getRedeemCreditCard')
	Route.post('createorderredeem', 'RedeemController.postCreateOrderRedeem').formats(['json'])

	Route.get('redeem-history', 'UserController.index')

	Route.get('search', 'SearchController.getSearch')
	Route.post('postsearch', 'SearchController.postSearch')

	Route.get('login/facebook', 'AccountController.fbRedirect')
	Route.get('authenticated/facebook', 'AccountController.fbCallback')
	Route.get('login/google', 'AccountController.gpRedirect')
	Route.get('authenticated/google', 'AccountController.gpCallback')
	Route.get('register/facebook', 'AccountController.fbRegRedirect')
	Route.get('register/google', 'AccountController.gpRegRedirect')
})
// .middleware('auth')
// .middleware('roles')
.middleware('globalparam')

// Router for API
require('./api.js')
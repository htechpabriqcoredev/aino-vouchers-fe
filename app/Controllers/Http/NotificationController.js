'use strict'

const minifyHTML = require('./Helper/MinifyHTML.js')
const ApiService = require('./Helper/ApiService.js')
const MyHash = require('./Helper/Hash.js')
const Helpers = use('Helpers')
const Strings = require('./Helper/String.js')
const Logger = use('Logger')
const Env = use('Env')
const customCookie = require('cookie')
const ms = require('ms')
const { validate, validateAll } = use('Validator')

class NotificationController {
	async getNotification({ request, response, view, session }) {
		let user = session.get('user')
		let principal = session.get('principal')

		if(user) {
			const options = {
				headers: {
					Authorization: 'Bearer ' + user.token.token
				}
			}

			let settings = await ApiService.getData(Env.get('API_HOST') + '/getsetting')
			
			let meta = {
				title: 'Notifications',
				description: 'Notifications - ' + settings['data'][1]['value'],
				keywords: settings['data'][2]['value'],
				copyright: settings['data'][3]['value'],
				author: settings['data'][4]['value'],
				url: Env.get('BASE_URL'),
				image: Env.get('BASE_URL') + '/' + settings['data'][5]['value'],
				shortcutIcon: Env.get('IMAGE_HOST') + '/' + settings['data'][9]['value'],
				bigLogo: Env.get('IMAGE_HOST') + '/' + settings['data'][10]['value'],
				smallLogo: Env.get('IMAGE_HOST') + '/' + settings['data'][11]['value']
			}
			
			let template = view.render('notifications', {
				meta: meta,
				params: principal,
				data: {
					mod: 'notifications',
					user: user,
				}
			})
			
			return await minifyHTML.minify(template)
		} else {
			return response.redirect('/login')
		}
	}

	async postNotificationData({ request, response, view, session }) {
		let user = session.get('user')
		let principal = session.get('principal')
		let principal_id = session.get('principal_id')
		let req = request.all()

		let reqparam = {
			user_id: user['user']['id'],
			limit: req.limit,
			offset: req.offset,
			principal_id: principal_id
		}

		let notifications = await ApiService.getData(Env.get('API_HOST') + '/postusernotif' , 'POST', reqparam)
		
		return notifications['data']
	}
}

module.exports = NotificationController
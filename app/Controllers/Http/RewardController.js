'use strict'

const minifyHTML = require('./Helper/MinifyHTML.js')
const ApiService = require('./Helper/ApiService.js')
const MyHash = require('./Helper/Hash.js')
const Helpers = use('Helpers')
const Strings = require('./Helper/String.js')
const Logger = use('Logger')
const Env = use('Env')
const customCookie = require('cookie')
const ms = require('ms')
const moment = require('moment')
const { validate, validateAll } = use('Validator')

class RewardController {
	async getRewardCatalog({ request, response, view, session }) {
		let user = session.get('user')
		let principal = session.get('principal')
		let principal_id = session.get('principal_id')

		let reqparam = {
			principal: principal
		}

		let reqparam1 = {
			principal_id: principal_id
		}

		let settings = await ApiService.getData(Env.get('API_HOST') + '/getsetting')
		let bestsellers = await ApiService.getData(Env.get('API_HOST') + '/postbestsellerlimit/', 'POST', reqparam1)
		let ourpicks = await ApiService.getData(Env.get('API_HOST') + '/postourpickslimit/', 'POST', reqparam1)
		let banners = await ApiService.getData(Env.get('API_HOST') + '/postbanners/', 'POST', reqparam)

		let bestseller = bestsellers['data']
		let bestsellerarr = []
		for(let x in bestseller) {
			let nowDate = moment(new Date()).format('YYYY-MM-DD')
			let fnowDate = moment(nowDate).valueOf()
			let startDate = moment(bestseller[x]['date_start']).format('YYYY-MM-DD')
			let fstartDate = moment(startDate).valueOf()
			let endDate = moment(bestseller[x]['date_end']).format('YYYY-MM-DD')
			let fendDate = moment(endDate).valueOf()
			
			if(fstartDate <= fnowDate && fendDate >= fnowDate) {
				bestsellerarr.push({
					id: bestseller[x].id,
					reward_id: bestseller[x].reward_id,
					reward_id_encrypt: bestseller[x].reward_id ? await MyHash.encrypt(bestseller[x].reward_id.toString()) : '',
					date_start: bestseller[x].date_start,
					date_end: bestseller[x].date_end,
					loyalty_details_id: bestseller[x].loyalty_details_id,
					name: bestseller[x].name,
					point_required: bestseller[x].point_required,
					images: bestseller[x].images
				})
			}
		}

		let ourpick = ourpicks['data']
		let ourpickarr = []
		for(let x in ourpick) {
			let nowDate = moment(new Date()).format('YYYY-MM-DD')
			let fnowDate = moment(nowDate).valueOf()
			let startDate = moment(ourpick[x]['date_start']).format('YYYY-MM-DD')
			let fstartDate = moment(startDate).valueOf()
			let endDate = moment(ourpick[x]['date_end']).format('YYYY-MM-DD')
			let fendDate = moment(endDate).valueOf()

			if(fstartDate <= fnowDate && fendDate >= fnowDate) {
				ourpickarr.push({
					id: ourpick[x].id,
					reward_id: ourpick[x].reward_id,
					reward_id_encrypt: ourpick[x].reward_id ? await MyHash.encrypt(ourpick[x].reward_id.toString()) : '',
					date_start: ourpick[x].date_start,
					date_end: ourpick[x].date_end,
					loyalty_details_id: ourpick[x].loyalty_details_id,
					name: ourpick[x].name,
					point_required: ourpick[x].point_required,
					images: ourpick[x].images
				})
			}
		}

		let banner = banners['data']
		let bannerarr = []
		for(let x in banner) {
			bannerarr.push({
				id: banner[x].id,
				user_id: banner[x].user_id,
				start_date: banner[x].start_date,
				start_hour: banner[x].start_hour,
				end_date: banner[x].end_date,
				end_hour: banner[x].end_hour,
				picture: Env.get('IMAGE_HOST')+banner[x].picture,
				link: banner[x].link,
				reward_id: banner[x].reward_id,
				reward_id_encrypt: banner[x].reward_id ? await MyHash.encrypt(banner[x].reward_id.toString()) : ''
			})
		}
		
		let meta = {
			title: 'Reward Catalog',
			description: 'Reward Catalog - ' + settings['data'][1]['value'],
			keywords: settings['data'][2]['value'],
			copyright: settings['data'][3]['value'],
			author: settings['data'][4]['value'],
			url: Env.get('BASE_URL'),
			image: Env.get('BASE_URL') + '/' + settings['data'][5]['value'],
			shortcutIcon: Env.get('IMAGE_HOST') + '/' + settings['data'][9]['value'],
			bigLogo: Env.get('IMAGE_HOST') + '/' + settings['data'][10]['value'],
			smallLogo: Env.get('IMAGE_HOST') + '/' + settings['data'][11]['value']
		}
		
		let template = view.render('reward-catalog', {
			meta: meta,
			params: principal,
			data: {
				mod: 'reward-catalog',
				user: user,
				bestsellers: bestsellerarr,
				ourpicks: ourpickarr,
				banners: bannerarr
			}
		})
		
		return await minifyHTML.minify(template)
	}

	async getRewardCategories({ request, response, view, session }) {
		let user = session.get('user')
		let principal = session.get('principal')
		const { code } = request.only(['code'])

		const url = request.url()
		session.forget('urlpanel')
		session.put('urlpanel', url)

		let settings = await ApiService.getData(Env.get('API_HOST') + '/getsetting')
		
		let meta = {
			title: 'Reward Categories',
			description: 'Reward Categories - ' + settings['data'][1]['value'],
			keywords: settings['data'][2]['value'],
			copyright: settings['data'][3]['value'],
			author: settings['data'][4]['value'],
			url: Env.get('BASE_URL'),
			image: Env.get('BASE_URL') + '/' + settings['data'][5]['value'],
			shortcutIcon: Env.get('IMAGE_HOST') + '/' + settings['data'][9]['value'],
			bigLogo: Env.get('IMAGE_HOST') + '/' + settings['data'][10]['value'],
			smallLogo: Env.get('IMAGE_HOST') + '/' + settings['data'][11]['value']
		}
		
		let template = view.render('reward-categories', {
			meta: meta,
			params: principal,
			data: {
				mod: 'rewards-categories',
				user: user,
			}
		})
		
		return await minifyHTML.minify(template)
	}

	async getMyReward({ request, response, view, session }) {
		let user = session.get('user')
		let principal = session.get('principal')
		const { code } = request.only(['code'])

		if(user) {
			const options = {
				headers: {
					Authorization: 'Bearer ' + user.token.token
				}
			}

			let settings = await ApiService.getData(Env.get('API_HOST') + '/getsetting')
			let customer = await ApiService.getData(Env.get('API_HOST') + '/getcustomerdetail/' + user['user']['id'], 'GET', null, options)
		
			let meta = {
				title: 'My Reward',
				description: 'My Reward - ' + settings['data'][1]['value'],
				keywords: settings['data'][2]['value'],
				copyright: settings['data'][3]['value'],
				author: settings['data'][4]['value'],
				url: Env.get('BASE_URL'),
				image: Env.get('BASE_URL') + '/' + settings['data'][5]['value'],
				shortcutIcon: Env.get('IMAGE_HOST') + '/' + settings['data'][9]['value'],
				bigLogo: Env.get('IMAGE_HOST') + '/' + settings['data'][10]['value'],
				smallLogo: Env.get('IMAGE_HOST') + '/' + settings['data'][11]['value']
			}
			
			let template = view.render('my-rewards', {
				meta: meta,
				params: principal,
				data: {
					mod: 'my-rewards',
					user: user,
					customer: customer['data'],
				}
			})
			
			return await minifyHTML.minify(template)
		} else {
			return response.redirect('/login')
		}
	}

	async postMyRewardData({request, response, view, session}) {
		let req = request.all()
		let user = session.get('user')
		let principal = session.get('principal')

		let reqparam = {
			user_id: user['user']['id'],
			limit: req.limit,
			offset: req.offset,
			principal: principal,
			tag: req.tag
		}

		let myrewards = await ApiService.getData(Env.get('API_HOST') + '/postmyreward', 'POST', reqparam)
		
		let myreward = myrewards['data']
		let content = []
		for(let x in myreward) {
			content.push({
				id : myreward[x].id,
				status: myreward[x].status,
				name: myreward[x].name,
				point_required: myreward[x].point_required,
				expiry_date: myreward[x].expiry_date,
				images: myreward[x].images,
				reward_id: myreward[x].reward_id,
				reward_id_encrypt: await MyHash.encrypt((myreward[x].reward_id).toString()),
			})
		}
		
		return content
	}

	async getMyRewardDetail({ request, response, view, session }) {
		let user = session.get('user')
		let principal = session.get('principal')
		const { id } = request.only(['id'])

		if(user) {
			const options = {
				headers: {
					Authorization: 'Bearer ' + user.token.token
				}
			}

			let reqparam = {
				reward_id: id
			}

			let settings = await ApiService.getData(Env.get('API_HOST') + '/getsetting')
			let rewarddetails = await ApiService.getData(Env.get('API_HOST') + '/postrewarddetails', 'POST', reqparam)
			
			let meta = {
				title: 'My Reward',
				description: 'My Reward - ' + settings['data'][1]['value'],
				keywords: settings['data'][2]['value'],
				copyright: settings['data'][3]['value'],
				author: settings['data'][4]['value'],
				url: Env.get('BASE_URL'),
				image: Env.get('BASE_URL') + '/' + settings['data'][5]['value'],
				shortcutIcon: Env.get('IMAGE_HOST') + '/' + settings['data'][9]['value'],
				bigLogo: Env.get('IMAGE_HOST') + '/' + settings['data'][10]['value'],
				smallLogo: Env.get('IMAGE_HOST') + '/' + settings['data'][11]['value']
			}
			
			let template = view.render('my-reward-details', {
				meta: meta,
				params: principal,
				data: {
					mod: 'my-reward-details',
					user: user,
					rewards: rewarddetails['data']
				}
			})
			
			return await minifyHTML.minify(template)
		} else {
			return response.redirect('/home/'+principal)
		}
	}

	async getUseRewardDetail({ request, response, view, session }) {
		let user = session.get('user')
		let principal = session.get('principal')
		const { id } = request.only(['id'])
		if(user) {
			const options = {
				headers: {
					Authorization: 'Bearer ' + user.token.token
				}
			}
			let reqparam = {
				id: id
			}
			let settings = await ApiService.getData(Env.get('API_HOST') + '/getsetting')
			let rewarddetails = await ApiService.getData(Env.get('API_HOST') + '/postuserewarddetails', 'POST', reqparam)
			let meta = {
				title: 'My Reward',
				description: 'My Reward - ' + settings['data'][1]['value'],
				keywords: settings['data'][2]['value'],
				copyright: settings['data'][3]['value'],
				author: settings['data'][4]['value'],
				url: Env.get('BASE_URL'),
				image: Env.get('BASE_URL') + '/' + settings['data'][5]['value'],
				shortcutIcon: Env.get('IMAGE_HOST') + '/' + settings['data'][9]['value'],
				bigLogo: Env.get('IMAGE_HOST') + '/' + settings['data'][10]['value'],
				smallLogo: Env.get('IMAGE_HOST') + '/' + settings['data'][11]['value']
			}
			let template = view.render('my-reward-details-use', {
				meta: meta,
				params: principal,
				data: {
					mod: 'my-reward-details-use',
					user: user,
					rewards: rewarddetails['data']
				}
			})
			return await minifyHTML.minify(template)
		} else {
			return response.redirect('/home/'+principal)
		}
	}

	async getRewardDetail({ params, request, response, view, session }) {
		let user = session.get('user')
		let principal = session.get('principal')
		let hashpanel = session.get('hashpanel')
		let urlpanel = session.get('urlpanel')
		const { id, fr } = request.only(['id', 'fr'])

		const url = request.originalUrl()
		session.forget('url')
		session.put('url', url)

		let reqparam = {
			reward_id: id
		}

		let settings = await ApiService.getData(Env.get('API_HOST') + '/getsetting')
		let meta = {
			title: 'Reward Details',
			description: 'Reward Details - ' + settings['data'][1]['value'],
			keywords: settings['data'][2]['value'],
			copyright: settings['data'][3]['value'],
			author: settings['data'][4]['value'],
			url: Env.get('BASE_URL'),
			image: Env.get('BASE_URL') + '/' + settings['data'][5]['value'],
			shortcutIcon: Env.get('IMAGE_HOST') + '/' + settings['data'][9]['value'],
			bigLogo: Env.get('IMAGE_HOST') + '/' + settings['data'][10]['value'],
			smallLogo: Env.get('IMAGE_HOST') + '/' + settings['data'][11]['value']
		}
		let rewarddetails = await ApiService.getData(Env.get('API_HOST') + '/postrewarddetails', 'POST', reqparam)
			

		if(user){
			
			let userpoint = await ApiService.getData(Env.get('API_HOST') + '/getuserpoint/' + user['user']['id'], 'GET', null)
			
			let template = view.render('reward-details', {
				meta: meta,
				params: principal,
				paramsFr: fr,
				paramsUrl: urlpanel + hashpanel,
				data: {
					mod: 'reward-details',
					user: user,
					reward_id_encrypt: id,
					rewards: rewarddetails['data'],
					userpoint: userpoint['data']
				}
			})

			return await minifyHTML.minify(template)
		} else {
			let template = view.render('reward-details', {
				meta: meta,
				params: principal,
				paramsFr: fr,
				paramsUrl: urlpanel + hashpanel,
				data: {
					mod: 'reward-details',
					user: user,
					reward_id_encrypt: id,
					rewards: rewarddetails['data'],
					userpoint: ''
				}
			})

			return await minifyHTML.minify(template)
		}	
	}

	async getOurPicks({ request, response, view, session }) {
		let user = session.get('user')
		let principal = session.get('principal')

		let settings = await ApiService.getData(Env.get('API_HOST') + '/getsetting')
	
		let meta = {
			title: 'Our Picks',
			description: 'Our Picks - ' + settings['data'][1]['value'],
			keywords: settings['data'][2]['value'],
			copyright: settings['data'][3]['value'],
			author: settings['data'][4]['value'],
			url: Env.get('BASE_URL'),
			image: Env.get('BASE_URL') + '/' + settings['data'][5]['value'],
			shortcutIcon: Env.get('IMAGE_HOST') + '/' + settings['data'][9]['value'],
			bigLogo: Env.get('IMAGE_HOST') + '/' + settings['data'][10]['value'],
			smallLogo: Env.get('IMAGE_HOST') + '/' + settings['data'][11]['value']
		}

		let template

		if(user) {
			const options = {
				headers: {
					Authorization: 'Bearer ' + user.token.token
				}
			}
			
			template = view.render('reward-ourpicks', {
				meta: meta,
				params: principal,
				data: {
					mod: 'our-picks',
					user: user,
				}
			})
		} else {
			template = view.render('reward-ourpicks', {
				meta: meta,
				params: principal,
				data: {
					mod: 'our-picks',
				}
			})
		}
		return await minifyHTML.minify(template)
	}

	async getBestSeller({ request, response, view, session }) {
		let user = session.get('user')
		let principal = session.get('principal')

		let settings = await ApiService.getData(Env.get('API_HOST') + '/getsetting')
	
		let meta = {
			title: 'Best Seller',
			description: 'Best Seller - ' + settings['data'][1]['value'],
			keywords: settings['data'][2]['value'],
			copyright: settings['data'][3]['value'],
			author: settings['data'][4]['value'],
			url: Env.get('BASE_URL'),
			image: Env.get('BASE_URL') + '/' + settings['data'][5]['value'],
			shortcutIcon: Env.get('IMAGE_HOST') + '/' + settings['data'][9]['value'],
			bigLogo: Env.get('IMAGE_HOST') + '/' + settings['data'][10]['value'],
			smallLogo: Env.get('IMAGE_HOST') + '/' + settings['data'][11]['value']
		}

		let template

		if(user) {
			const options = {
				headers: {
					Authorization: 'Bearer ' + user.token.token
				}
			}

			let settings = await ApiService.getData(Env.get('API_HOST') + '/getsetting')
			
			template = view.render('reward-bestseller', {
				meta: meta,
				params: principal,
				data: {
					mod: 'best-seller',
					user: user,
				}
			})
		} else {
			template = view.render('reward-bestseller', {
				meta: meta,
				params: principal,
				data: {
					mod: 'best-seller',
				}
			})
		}

		return await minifyHTML.minify(template)
	}

	async postBestSellerData({request, response, view, session}) {
		let req = request.all()
		let principal_id = session.get('principal_id')

		let reqparam = {
			limit: req.limit,
			offset: req.offset,
			principal_id: principal_id
		}

		let bestsellers = await ApiService.getData(Env.get('API_HOST') + '/postbestseller', 'POST', reqparam)
		
		let bestseller = bestsellers['data']
		let content = []

		for(let x in bestseller) {
			let nowDate = moment(new Date()).format('YYYY-MM-DD')
			let fnowDate = moment(nowDate).valueOf()
			let startDate = moment(bestseller[x]['date_start']).format('YYYY-MM-DD')
			let fstartDate = moment(startDate).valueOf()
			let endDate = moment(bestseller[x]['date_end']).format('YYYY-MM-DD')
			let fendDate = moment(endDate).valueOf()

			if(fstartDate <= fnowDate && fendDate >= fnowDate) {
				content.push({
					reward_id: bestseller[x]['reward_id'],
					reward_id_encrypt: await MyHash.encrypt(bestseller[x]['reward_id'].toString()),
					name: bestseller[x]['name'],
					point_required: bestseller[x]['point_required'],
					images: bestseller[x]['images'] ? bestseller[x]['images'] : ''
				})
			}
		}
		
		return content
	}

	async postOurPicksData({request, response, view, session}) {
		let req = request.all()
		let principal_id = session.get('principal_id')

		let reqparam = {
			limit: req.limit,
			offset: req.offset,
			principal_id: principal_id
		}

		let ourpicks = await ApiService.getData(Env.get('API_HOST') + '/postourpicks', 'POST', reqparam)
		
		let ourpick = ourpicks['data']
		let content = []

		for(let x in ourpick) {
			let nowDate = moment(new Date()).format('YYYY-MM-DD')
			let fnowDate = moment(nowDate).valueOf()
			let startDate = moment(ourpick[x]['date_start']).format('YYYY-MM-DD')
			let fstartDate = moment(startDate).valueOf()
			let endDate = moment(ourpick[x]['date_end']).format('YYYY-MM-DD')
			let fendDate = moment(endDate).valueOf()

			if(fstartDate <= fnowDate && fendDate >= fnowDate) {
				content.push({
					reward_id: ourpick[x]['reward_id'],
					reward_id_encrypt: await MyHash.encrypt(ourpick[x]['reward_id'].toString()),
					name: ourpick[x]['name'],
					point_required: ourpick[x]['point_required'],
					images: ourpick[x]['images'] ? ourpick[x]['images'] : ''
				})
			}
		}
		
		return content
	}

	async postCategoriesData({ params, request, response, view, session }) {
		let user = session.get('user')
		let principal_id = session.get('principal_id')

		let req = request.post()

		session.forget('hashpanel')
		session.put('hashpanel', req.taghash)	

		let filters = []
		filters.push({
			key: 'rewards.category_name',
			operator: '=',
			value: req.category
		})

		if(req.tagfilt == 1) {
			filters.push({
				key: 'rewards.point_required',
				operator: '<',
				value: 250
			})
		} else if(req.tagfilt == 2) {
			filters.push({
				key: 'rewards.point_required',
				operator: '>=',
				value: 250
			})
			filters.push({
				key: 'rewards.point_required',
				operator: '<=',
				value: 500
			})
		} else if(req.tagfilt == 3) {
			filters.push({
				key: 'rewards.point_required',
				operator: '>=',
				value: 500
			})
			filters.push({
				key: 'rewards.point_required',
				operator: '<=',
				value: 750
			})
		} else if(req.tagfilt == 4) {
			filters.push({
				key: 'rewards.point_required',
				operator: '>',
				value: 1000
			})
		}

		let sortby
		if(req.tagsort == 1) {
			sortby = {
				key: 'count(transaction_burns.id)',
				operator: 'desc'
			}
		} else if(req.tagsort == 2) {
			sortby = {
				key: 'loyalty_details.expiry_date',
				operator: 'desc'
			}
		} else if(req.tagsort == 3) {
			sortby = {
				key: 'rewards.point_required',
				operator: 'asc'
			}
		} else if(req.tagsort == 4) {
			sortby = {
				key: 'rewards.point_required',
				operator: 'desc'
			}
		}

		let reqparam = {
			limit: req.limit,
			offset: req.offset,
			filter: filters,
			sortby: sortby,
			principal_id: principal_id,
		}

		let categorycontent = await ApiService.getData(Env.get('API_HOST') + '/findreward/' , 'POST', reqparam)
		let content = categorycontent['data']
		let contents = []

		for(let x in content) {
			contents.push({
				id: content[x]['id'],
				id_encrypt: await MyHash.encrypt(content[x]['id'].toString()),
				name: content[x]['name'],
				point_required: content[x]['point_required'],
				images: content[x]['images'] ? content[x]['images'] : '',
				expiry_date: content[x]['expiry_date'],
				location: content[x]['location'],
				loyalty_detail_id: content[x]['loyalty_detail_id'],
				response: content[x]['response'],
				status: content[x]['status'],
				term_condition: content[x]['term_condition'],
				quantity: content[x]['quantity']
			})
		}

		if (categorycontent['code'] == '2000') {
			let data = {
				code: '2000',
				message: 'Category Data Found',
				data: contents
			}

			return data
		} else {
			let data = {
				code: categorycontent['code'],
				message: 'Category Data Not Found',
				data: []
			}

			return data
		}
	}

	async postCategoriesNewDealsData({ params, request, response, view, session }) {
		let user = session.get('user')
		let principal_id = session.get('principal_id')

		let req = request.post()

		session.forget('hashpanel')
		session.put('hashpanel', req.taghash)	

		let filters = []

		if(req.tagfilt == 1) {
			filters.push({
				key: 'rewards.point_required',
				operator: '<',
				value: 250
			})
		} else if(req.tagfilt == 2) {
			filters.push({
				key: 'rewards.point_required',
				operator: '>=',
				value: 250
			})
			filters.push({
				key: 'rewards.point_required',
				operator: '<=',
				value: 500
			})
		} else if(req.tagfilt == 3) {
			filters.push({
				key: 'rewards.point_required',
				operator: '>=',
				value: 500
			})
			filters.push({
				key: 'rewards.point_required',
				operator: '<=',
				value: 750
			})
		} else if(req.tagfilt == 4) {
			filters.push({
				key: 'rewards.point_required',
				operator: '>',
				value: 1000
			})
		}

		let sortby
		if(req.tagsort == 1) {
			sortby = {
				key: 'count(transaction_burns.id)',
				operator: 'desc'
			}
		} else if(req.tagsort == 2) {
			sortby = {
				key: 'loyalty_details.expiry_date',
				operator: 'desc'
			}
		} else if(req.tagsort == 3) {
			sortby = {
				key: 'rewards.point_required',
				operator: 'asc'
			}
		} else if(req.tagsort == 4) {
			sortby = {
				key: 'rewards.point_required',
				operator: 'desc'
			}
		}

		let reqparam = {
			limit: req.limit,
			offset: req.offset,
			filter: filters,
			sortby: sortby,
			principal_id: principal_id,
		}

		let categorycontent = await ApiService.getData(Env.get('API_HOST') + '/findnewdealsreward/' , 'POST', reqparam)
		let content = categorycontent['data']
		let contents = []

		for(let x in content) {
			contents.push({
				id: content[x]['id'],
				id_encrypt: await MyHash.encrypt(content[x]['id'].toString()),
				name: content[x]['name'],
				point_required: content[x]['point_required'],
				images: content[x]['images'] ? content[x]['images'] : '',
				expiry_date: content[x]['expiry_date'],
				location: content[x]['location'],
				loyalty_detail_id: content[x]['loyalty_detail_id'],
				response: content[x]['response'],
				status: content[x]['status'],
				term_condition: content[x]['term_condition'],
				quantity: content[x]['quantity']
			})
		}

		if (categorycontent['code'] == '2000') {
			let data = {
				code: '2000',
				message: 'Category Data Found',
				data: contents
			}

			return data
		} else {
			let data = {
				code: categorycontent['code'],
				message: 'Category Data Not Found',
				data: []
			}

			return data
		}
	}
}

module.exports = RewardController
'use strict'

const minifyHTML = require('./Helper/MinifyHTML.js')
const ApiService = require('./Helper/ApiService.js')
const MyHash = require('./Helper/Hash.js')
const Helpers = use('Helpers')
const Strings = require('./Helper/String.js')
const Logger = use('Logger')
const Env = use('Env')
const customCookie = require('cookie')
const ms = require('ms')
const moment = require('moment')
const { validate, validateAll } = use('Validator')

class SearchController {
	async getSearch({ params, request, response, view, session }) {
		let principal = session.get('principal')
		let { key } = request.only(['key'])

		let settings = await ApiService.getData(Env.get('API_HOST') + '/getsetting')
		
		let meta = {
			title: 'Search',
			description: 'Search - ' + settings['data'][1]['value'],
			keywords: settings['data'][2]['value'],
			copyright: settings['data'][3]['value'],
			author: settings['data'][4]['value'],
			url: Env.get('BASE_URL'),
			image: Env.get('BASE_URL') + '/' + settings['data'][5]['value'],
			shortcutIcon: Env.get('IMAGE_HOST') + '/' + settings['data'][9]['value'],
			bigLogo: Env.get('IMAGE_HOST') + '/' + settings['data'][10]['value'],
			smallLogo: Env.get('IMAGE_HOST') + '/' + settings['data'][11]['value']
		}
		
		let template = view.render('search', {
			meta: meta,
			params: principal,
			data: {
				mod: 'search',
				key: key
			}
		})
		
		return await minifyHTML.minify(template)
	}

	async postSearch({request, response, view, session}) {
		let req = request.all()
		let principal_id = session.get('principal_id')

		let reqparam = {
			limit: req.limit,
			offset: req.offset,
			principal_id: principal_id,
			find: req.find
		}

		let searchdatas = await ApiService.getData(Env.get('API_HOST') + '/findreward', 'POST', reqparam)
		
		let searchdata = searchdatas['data']
		let content = []

		for(let x in searchdata) {
			content.push({
				id: searchdata[x].id,
				id_encrypt: await MyHash.encrypt(searchdata[x]['id'].toString()),
				name: searchdata[x].name,
				point_required: searchdata[x].point_required
			})
		}
		
		return content
	}
}

module.exports = SearchController
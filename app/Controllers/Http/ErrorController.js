'use strict'

const Database = use('Database')
const Env = use('Env')
const minifyHTML = require('./Helper/MinifyHTML.js')

class ErrorController {
	async index({ params, request, response, auth, view}) {
		let template = view.render('error.404')
		
		return await minifyHTML.minify(template)
	}

	async notFound({ request, response, view, session }) {
		let cacheTemplate
		let pageIp
		let getPageIp = request.cookie('aino-session')
		if (getPageIp) {
			pageIp = getPageIp
		} else {
			pageIp = request.ip().replace(/[.]/g, '')
		}
		if (await Cache.tags(pageIp).get('page-error')) {
			cacheTemplate = await Cache.tags(pageIp).get('page-error')
		} else {
			let settings = await ApiService.getData(Env.get('API_HOST') + '/getsetting')
			
			let user = session.get('user')
			
			let meta = {
				title: 'Not Found - ' + settings['data'][0]['value'],
				description: 'Not Found - ' + settings['data'][1]['value'],
				keywords: settings['data'][2]['value'],
				copyright: settings['data'][3]['value'],
				author: settings['data'][4]['value'],
				url: Env.get('BASE_URL'),
				image: Env.get('BASE_URL') + '/' + settings['data'][5]['value'],
				shortcutIcon: Env.get('IMAGE_HOST') + '/' + settings['data'][9]['value'],
				bigLogo: Env.get('IMAGE_HOST') + '/' + settings['data'][10]['value'],
				smallLogo: Env.get('IMAGE_HOST') + '/' + settings['data'][11]['value']
			}
			
			let template = view.render('error.404', {
				meta: meta,
				data: {
					user: user
				}
			})
			
			cacheTemplate = await minifyHTML.minify(template)
			await Cache.tags(pageIp).put('page-error', cacheTemplate, 5)
		}
		
		return cacheTemplate
	}
}

module.exports = ErrorController
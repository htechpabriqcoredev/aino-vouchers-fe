'use strict'

const User = use('App/Models/User')
const Setting = use('App/Models/Setting')
const ApiService = require('./Helper/ApiService.js')
const Logger = use('Logger')
const Env = use('Env')
const moment = use('moment')
const minifyHTML = require('./Helper/MinifyHTML.js')

class HomeController {
	async index({ params, request, response, view, session }) {
		let cacheTemplate
		let template

		let settings = await ApiService.getData(Env.get('API_HOST') + '/getsetting')

		let meta = {
			title: 'Home - ' + settings['data'][0]['value'],
			description: settings['data'][1]['value'],
			keywords: settings['data'][2]['value'],
			copyright: settings['data'][3]['value'],
			author: settings['data'][4]['value'],
			url: Env.get('BASE_URL'),
			image: Env.get('BASE_URL') + '/' + settings['data'][5]['value'],
			shortcutIcon: Env.get('IMAGE_HOST') + '/' + settings['data'][9]['value'],
			bigLogo: Env.get('IMAGE_HOST') + '/' + settings['data'][10]['value'],
			smallLogo: Env.get('IMAGE_HOST') + '/' + settings['data'][11]['value']
		}

		let user = session.get('user')
		let userpoint
		if(user) {
			const options = {
				headers: {
					Authorization: 'Bearer ' + user.token.token
				}
			}
			userpoint = await ApiService.getData(Env.get('API_HOST') + '/getuserpoint/' + user['user']['id'], 'GET', null, options)
		}

		template = view.render('home', {
			meta: meta,
			params: params.state,
			data: {
				user: user ? user : null,
				// userpoint: userpoint ? userpoint['data'] : 0
				userpoint: userpoint ? 0 : 0
			}
		})

		cacheTemplate = await minifyHTML.minify(template)
		// await Cache.tags(pageIp).put('page-home', cacheTemplate, 5)
		return cacheTemplate
	}

	async getHome({ params, request, response, view, session }) {
		let cacheTemplate
		let template

		session.forget('principal')
		session.forget('principal_id')
		session.forget('url')
		session.put('principal', params.state)

		let reqparam = {
			principal: params.state
		}

		let settings = await ApiService.getData(Env.get('API_HOST') + '/getsetting')
		let principals = await ApiService.getData(Env.get('API_HOST') + '/postprincipalid/', 'POST', reqparam)
		session.put('principal_id', principals['data']['principal_id'])

		let meta = {
			title: 'Home - ' + settings['data'][0]['value'],
			description: settings['data'][1]['value'],
			keywords: settings['data'][2]['value'],
			copyright: settings['data'][3]['value'],
			author: settings['data'][4]['value'],
			url: Env.get('BASE_URL'),
			image: Env.get('BASE_URL') + '/' + settings['data'][5]['value'],
			shortcutIcon: Env.get('IMAGE_HOST') + '/' + settings['data'][9]['value'],
			bigLogo: Env.get('IMAGE_HOST') + '/' + settings['data'][10]['value'],
			smallLogo: Env.get('IMAGE_HOST') + '/' + settings['data'][11]['value']
		}

		let user = session.get('user')
		let principal = session.get('principal')
		let userpoint
		let customer
		let newuser
		if(user) {
			if(params.state == principal) {
				const options = {
					headers: {
						Authorization: 'Bearer ' + user.token.token
					}
				}
				userpoint = await ApiService.getData(Env.get('API_HOST') + '/getuserpoint/' + user['user']['id'], 'GET', null, options)
				customer = await ApiService.getData(Env.get('API_HOST') + '/getcustomerdetail/' + user['user']['id'], 'GET', null, options)
				newuser = await ApiService.getData(Env.get('API_HOST') + '/getuserdetail/' + user['user']['id'], 'GET', null, options)
			} else {
				let template = view.render('error.404', {
					meta: meta,
					params: principal
				})
				
				return await minifyHTML.minify(template)
			}
		}
		
		template = view.render('home', {
			meta: meta,
			params: principal ? principal : params.state,
			data: {
				user: user ? user : null,
				// userpoint: userpoint ? userpoint['data'] : 0
				userpoint: userpoint ? userpoint['data'] : 0,
				customer: customer ? customer['data'] : null,
				newuser: newuser ? newuser['data'] : null
			}
		})

		cacheTemplate = await minifyHTML.minify(template)
		// await Cache.tags(pageIp).put('page-home', cacheTemplate, 5)
		return cacheTemplate
	}

	async getDashboard({ params, request, response, auth, view }) {
		let settings = await ApiService.getData(Env.get('API_HOST') + '/getsetting')
				
		let meta = {
			title: settings['data'][0]['value'],
			description: settings['data'][1]['value'],
			keywords: settings['data'][2]['value'],
			copyright: settings['data'][3]['value'],
			author: settings['data'][4]['value'],
			url: Env.get('BASE_URL'),
			image: Env.get('BASE_URL') + '/' + settings['data'][5]['value'],
			shortcutIcon: Env.get('IMAGE_HOST') + '/' + settings['data'][9]['value'],
			bigLogo: Env.get('IMAGE_HOST') + '/' + settings['data'][10]['value'],
			smallLogo: Env.get('IMAGE_HOST') + '/' + settings['data'][11]['value']
		}
		
		let template = view.render('dashboard', {
			meta: meta
		})
		
		return await minifyHTML.minify(template)
	}

	async notFound({ params, request, response, auth, view }) {
		let settings = await ApiService.getData(Env.get('API_HOST') + '/getsetting')
				
		let meta = {
			title: settings['data'][0]['value'],
			description: settings['data'][1]['value'],
			keywords: settings['data'][2]['value'],
			copyright: settings['data'][3]['value'],
			author: settings['data'][4]['value'],
			url: Env.get('BASE_URL'),
			image: Env.get('BASE_URL') + '/' + settings['data'][5]['value'],
			shortcutIcon: Env.get('IMAGE_HOST') + '/' + settings['data'][9]['value'],
			bigLogo: Env.get('IMAGE_HOST') + '/' + settings['data'][10]['value'],
			smallLogo: Env.get('IMAGE_HOST') + '/' + settings['data'][11]['value']
		}
		
		let template = view.render('error.404', {
			meta: meta
		})
		
		return await minifyHTML.minify(template)
	}
}

module.exports = HomeController
'use strict'

const User = use('App/Models/User')
const minifyHTML = require('./Helper/MinifyHTML.js')
const ApiService = require('./Helper/ApiService.js')
const Helpers = use('Helpers')
const Logger = use('Logger')
const Env = use('Env')
const customCookie = require('cookie')
const ms = require('ms')
const { validate, validateAll } = use('Validator')
const delay = require('delay')
const Cache = use('Cache')

class AccountController {
	async getIndex({ request, response, view, session }) {
		let user = session.get('user')
		if (user) {
			return response.redirect('/')
		} else {
			let settings = await ApiService.getData(Env.get('API_HOST') + '/getsetting')
				
			let meta = {
				title: 'Login - ' + settings['data'][0]['value'],
				description: settings['data'][1]['value'],
				keywords: settings['data'][2]['value'],
				copyright: settings['data'][3]['value'],
				author: settings['data'][4]['value'],
				url: Env.get('BASE_URL'),
				image: Env.get('BASE_URL') + '/' + settings['data'][5]['value'],
				shortcutIcon: Env.get('IMAGE_HOST') + '/' + settings['data'][9]['value'],
				bigLogo: Env.get('IMAGE_HOST') + '/' + settings['data'][10]['value'],
				smallLogo: Env.get('IMAGE_HOST') + '/' + settings['data'][11]['value']
			}
			
			let template = view.render('home', {
				meta: meta,
				data: {}
			})
			
			return await minifyHTML.minify(template)
		}
	}

	async getLogin({ params, request, response, view, session }) {
		let principal = session.get('principal')
		let user = session.get('user')

		let reqparam = {
			principal: principal
		}
		if (user) {
			return response.redirect('/home/'+principal)
		} else {
			let settings = await ApiService.getData(Env.get('API_HOST') + '/getsetting')
			let themes = await ApiService.getData(Env.get('API_HOST') + '/postthemeprincipal', 'POST', reqparam)

			let theme = {
				logo: Env.get('IMAGE_HOST')+themes['data']['logo']	
			}
			let meta = {
				title: 'Login - ' + settings['data'][0]['value'],
				description: settings['data'][1]['value'],
				keywords: settings['data'][2]['value'],
				copyright: settings['data'][3]['value'],
				author: settings['data'][4]['value'],
				url: Env.get('BASE_URL'),
				image: Env.get('BASE_URL') + '/' + settings['data'][5]['value'],
				shortcutIcon: Env.get('IMAGE_HOST') + '/' + settings['data'][9]['value'],
				bigLogo: Env.get('IMAGE_HOST') + '/' + settings['data'][10]['value'],
				smallLogo: Env.get('IMAGE_HOST') + '/' + settings['data'][11]['value']
			}
			let template = view.render('login', {
				meta: meta,
				params: principal,
				themes: theme,
			})
			return await minifyHTML.minify(template)
		}
	}

	async postLogin({ params, request, response, view, session }) {
		const { email, password, remember_me, code } = request.only(['email', 'password', 'remember_me', 'code'])
		let url = session.get('url')
		let principal = session.get('principal')

		const reqparam = {
			email: email,
			password: password,
			principal: principal
		}

		const rules = {
			email: 'required',
			password: 'required',
		}

		const messages = {
			'email.required': 'Enter your phone number or email',
			'password.required': 'Enter your password'
		}

		const validation = await validateAll(reqparam, rules, messages)
		if (validation.fails()) {
			session.withErrors(validation.messages())
			return response.redirect('back')
		} else {
			let login = await ApiService.getData(Env.get('API_HOST') + '/login', 'POST', reqparam)
			
			if (login['code'] == '2000') {
				session.put('user', login['data'])
				if (remember_me == '1') {
					response.cookie('aino-rememberme', {
						email: email,
						password: password
					}, {
						httpOnly: true,
						sameSite: true,
						path: '/',
						expires: new Date(Date.now() + ms('30d'))
					})
				}
				let pageIp
				let getPageIp = request.cookie('aino-session')
				if (getPageIp) {
					pageIp = getPageIp
				} else {
					pageIp = request.ip().replace(/[.]/g, '')
				}
				await Cache.tags(pageIp).flush()
				await delay(1000)

				if(url != null) {
					return response.redirect(url)	
				} else {
					return response.redirect('/home/'+principal)
				}

				
			} else if(login['code'] == '4004') {
				session.flash({ notification: 'The password you entered is incorrect!' })
				return response.redirect('/login')
			} else if(login['code'] == '4005') {
				session.flash({ notification: 'Your email or telephone has not been registered!' })
				return response.redirect('/login')
			} else {
				session.flash({ notification: 'Oh snap! Something happen!' })
				return response.redirect('/login')
			}
		}
	}

	async gettAuthenticateSSO({ params, request, response, view, session }) {
		let authuser = params.secretcode
		const reqparam = {
			secretcode: authuser
		}
		const rules = {
			secretcode: 'required'
		}
		const messages = {
			'secretcode.required': 'Sorry your sso is wrong'
		}
		const validation = await validateAll(reqparam, rules, messages)
		if (validation.fails()) {
			session.withErrors(validation.messages())
			return response.redirect('back')
		} else {
			let login = await ApiService.getData(Env.get('API_HOST') + '/sso', 'POST', reqparam)
			if (login['code'] == '2000') {
				await session.put('user', login['data'])
				let pageIp
				let getPageIp = request.cookie('aino-session')
				if (getPageIp) {
					pageIp = getPageIp
				} else {
					pageIp = request.ip().replace(/[.]/g, '')
				}
				await Cache.tags(pageIp).flush()
				await delay(1000)
				return response.redirect('/home/'+ login['data'].user.principal_code)
			} else {
				session.flash({ notification: 'Oh snap! Something happen!' })
				return response.redirect('/login')
			}
		}
	}

	async fbRedirect({ ally }) {
		await ally.driver('facebook').redirect()
	}
	
	async fbCallback({ request, response, session, ally }) {
		let fbUser = await this.checkOAuth(ally, 'facebook')
		let url = session.get('url')
		let principal = session.get('principal')

		if (fbUser) {
			const reqparam = {
				email: fbUser.getEmail(),
				fullname: fbUser.getName(),
				social_token: fbUser.getAccessToken(),
				login_source: 'facebook',
				principal: principal
			}
			
			let login = await ApiService.getData(Env.get('API_HOST') + '/loginsocial', 'POST', reqparam)
			
			if (login['code'] == '2000') {
				session.put('user', login['data'])
				let pageIp
				let getPageIp = request.cookie('aino-session')
				if (getPageIp) {
					pageIp = getPageIp
				} else {
					pageIp = request.ip().replace(/[.]/g, '')
				}
				await Cache.tags(pageIp).flush()
				await delay(1000)
				return response.redirect('/home/'+principal)
			} else if (login['code'] == '2001') {
				let login = await ApiService.getData(Env.get('API_HOST') + '/loginsocial', 'POST', reqparam)
				
				if (login['code'] == '2000') {
					session.put('user', login['data'])
					let pageIp
					let getPageIp = request.cookie('aino-session')
					if (getPageIp) {
						pageIp = getPageIp
					} else {
						pageIp = request.ip().replace(/[.]/g, '')
					}
					await Cache.tags(pageIp).flush()
					await delay(1000)
					return response.redirect('/home/'+principal)
				} else {
					session.flash({ notification: 'Maaf...Terjadi kesalahan pada server!' })
					return response.redirect('/login')
				}
			} else {
				session.flash({ notification: 'Maaf...Terjadi kesalahan pada server!' })
				return response.redirect('/login')
			}
		} else {
			session.flash({ notification: 'Maaf...Terjadi kesalahan pada server!' })
			return response.redirect('/login')
		}
	}
	
	async gpRedirect({ ally }) {
		await ally.driver('google').redirect()
	}
	
	async gpCallback({ request, response, session, ally }) {
		let gpUser = await this.checkOAuth(ally, 'google')
		let url = session.get('url')
		let principal = session.get('principal')
		
		if (gpUser) {
			const reqparam = {
				email: gpUser.getEmail(),
				fullname: gpUser.getName(),
				social_token: gpUser.getAccessToken(),
				login_source: 'google',
				principal: principal
			}
			
			let login = await ApiService.getData(Env.get('API_HOST') + '/loginsocial', 'POST', reqparam)
			
			if (login['code'] == '2000') {
				session.put('user', login['data'])
				let pageIp
				let getPageIp = request.cookie('aino-session')
				if (getPageIp) {
					pageIp = getPageIp
				} else {
					pageIp = request.ip().replace(/[.]/g, '')
				}
				await Cache.tags(pageIp).flush()
				await delay(1000)
				return response.redirect('/home/'+principal)
			} else if (login['code'] == '2001') {
				let login = await ApiService.getData(Env.get('API_HOST') + '/loginsocial', 'POST', reqparam)
				
				if (login['code'] == '2000') {
					session.put('user', login['data'])
					let pageIp
					let getPageIp = request.cookie('aino-session')
					if (getPageIp) {
						pageIp = getPageIp
					} else {
						pageIp = request.ip().replace(/[.]/g, '')
					}
					await Cache.tags(pageIp).flush()
					await delay(1000)
					return response.redirect('/home/'+principal)
				} else {
					session.flash({ notification: 'Maaf...Terjadi kesalahan pada server!' })
					return response.redirect('/login')
				}
			} else {
				session.flash({ notification: 'Maaf...Terjadi kesalahan pada server!' })
				return response.redirect('/login')
			}
		} else {
			session.flash({ notification: 'Maaf...Terjadi kesalahan pada server!' })
			return response.redirect('/login')
		}
	}

	async getLogout({ request, response, auth }) {
		await auth.logout()
		return response.redirect('/')
	}
	
	async checkAuth(auth) {
		try {
			return await auth.check()
		} catch(error) {
			return false
		}
	}
	
	async checkUser(auth) {
		try {
			return await auth.getUser()
		} catch(error) {
			return false
		}
	}

	async forgotPassword({ request, response, auth, view, session }) {
		let user = session.get('user')
		let principal = session.get('principal')

		if (user) {
			return response.redirect('/home/'+principal)
		} else {
			let settings = await ApiService.getData(Env.get('API_HOST') + '/getsetting')

			let meta = {
				title: 'Forgot Password',
				description: 'Forgot Password - ' + settings['data'][1]['value'],
				keywords: settings['data'][2]['value'],
				copyright: settings['data'][3]['value'],
				author: settings['data'][4]['value'],
				url: Env.get('BASE_URL'),
				image: Env.get('BASE_URL') + '/' + settings['data'][5]['value'],
				shortcutIcon: Env.get('IMAGE_HOST') + '/' + settings['data'][9]['value'],
				bigLogo: Env.get('IMAGE_HOST') + '/' + settings['data'][10]['value'],
				smallLogo: Env.get('IMAGE_HOST') + '/' + settings['data'][11]['value']
			}
			
			let template = view.render('forgot-password', {
				meta: meta,
				data: {}
			})
			
			return await minifyHTML.minify(template)
		}
	}

	async postForgot({ request, response, view, session }) {
		const { email, telephone } = request.only(['email', 'telephone'])
		let principal = session.get('principal')
		
		const reqparam = {
			email: email,
			telephone: telephone,
			principal: principal,
			base_url: Env.get('BASE_URL')
		}
		
		let forgot = await ApiService.getData(Env.get('API_HOST') + '/forgot', 'POST', reqparam)
		
		if (forgot['code'] == '2000') {
			session.flash({ notification: 'Check your email or telephone for further instuctions!' })
			return 1
		} else {
			session.flash({ notification: 'Oh, Snap! Something happen!' })
			return 2
		}
	}

	async register({ params, request, response, view, session }) {
		let user = session.get('user')
		let principal = session.get('principal')
		let req = request.all()
		
		if (user) {
			return response.redirect('/home/'+req.code)
		} else {
			let settings = await ApiService.getData(Env.get('API_HOST') + '/getsetting')
			
			let meta = {
				title: 'Sign Up - ' + settings['data'][0]['value'],
				description: 'Sign Up - ' + settings['data'][1]['value'],
				keywords: settings['data'][2]['value'],
				copyright: settings['data'][3]['value'],
				author: settings['data'][4]['value'],
				url: Env.get('BASE_URL'),
				image: Env.get('BASE_URL') + '/' + settings['data'][5]['value'],
				shortcutIcon: Env.get('IMAGE_HOST') + '/' + settings['data'][9]['value'],
				bigLogo: Env.get('IMAGE_HOST') + '/' + settings['data'][10]['value'],
				smallLogo: Env.get('IMAGE_HOST') + '/' + settings['data'][11]['value']
			}
			
			let template = view.render('signup', {
				meta: meta,
				params: principal,
				data: {}
			})
			
			return await minifyHTML.minify(template)
		}
	}

	async postRegister({ request, response, view, session }) {
		const { fullname, email, telephone, password, confirm_password } = request.only(['fullname', 'email', 'telephone', 'password', 'confirm_password'])
		let principal = session.get('principal')
		await session.put('email', email)
		const reqparam = {
			fullname: fullname,
			email: email,
			telephone: telephone,
			password: password,
			confirm_password: confirm_password,
			principal: principal,
			base_url: Env.get('BASE_URL'),
		}
		
		const rules = {
			fullname: 'required',
			email: 'required',
			telephone: 'required',
			password: 'required',
			confirm_password: 'required'
		}
		
		const validation = await validateAll(reqparam, rules)
		
		if (validation.fails()) {
			session.withErrors(validation.messages()).flashExcept(['password', 'confirm_password'])
			return response.redirect('back')
		} else {
			if (password == confirm_password) {
				let register = await ApiService.getData(Env.get('API_HOST') + '/register', 'POST', reqparam)
				
				if (register['code'] == '2000') {
					session.flash({ notification: 'Registration Success, please check email for activation!' })
					return 1
				} else if(register['code'] == '4005') {
					session.flash({ notification: 'Email already registered!' })
					return 3
				} else if(register['code'] == '4006') {
					session.flash({ notification: 'Phone number already registered!' })
					return 5
				} else {
					session.flash({ notification: 'Check your data!' })
					return 2
				}
			} else {
				session.flash({ notification: 'Password is not match!' })
				return 4
			}
		}
	}

	async activation({ params, request, response, view, session }) {
		const reqparam = {
			id: params.id,
			key: params.key
		}

		let { pr } = request.only(['pr'])
		session.forget('principal')
		await session.put('principal', pr)
		
		let req = request.all()
		let activation = await ApiService.getData(Env.get('API_HOST') + '/activation', 'POST', reqparam)
			
		if (activation['code'] == '2000') {
			let settings = await ApiService.getData(Env.get('API_HOST') + '/getsetting')
			
			let user = session.get('user')
		
			let meta = {
				title: 'Activation - ' + settings['data'][0]['value'],
				description: 'Activation - ' + settings['data'][1]['value'],
				keywords: settings['data'][2]['value'],
				copyright: settings['data'][3]['value'],
				author: settings['data'][4]['value'],
				url: Env.get('BASE_URL'),
				image: Env.get('BASE_URL') + '/' + settings['data'][5]['value'],
				shortcutIcon: Env.get('IMAGE_HOST') + '/' + settings['data'][9]['value'],
				bigLogo: Env.get('IMAGE_HOST') + '/' + settings['data'][10]['value'],
				smallLogo: Env.get('IMAGE_HOST') + '/' + settings['data'][11]['value']
			}
			
			let template = view.render('activation', {
				meta: meta,
				paramsUrl: pr,
				data: {
					user: user,
					redir: Env.get('BASE_URL') + '/login'
				}
			})
			
			return await minifyHTML.minify(template)
		} else {
			return response.redirect('/home/'+pr)
		}
	}

	async reset({ params, request, response, view, session }) {
		const reqparam = {
			id: params.id,
			key: params.key
		}

		const { pr } = request.only(['pr'])
		
		if (params.key) {
			let settings = await ApiService.getData(Env.get('API_HOST') + '/getsetting')
			
			let user = session.get('user')

			let meta = {
				title: 'Reset Password - ' + settings['data'][0]['value'],
				description: 'Reset Password - ' + settings['data'][1]['value'],
				keywords: settings['data'][2]['value'],
				copyright: settings['data'][3]['value'],
				author: settings['data'][4]['value'],
				url: Env.get('BASE_URL'),
				image: Env.get('BASE_URL') + '/' + settings['data'][5]['value'],
				shortcutIcon: Env.get('IMAGE_HOST') + '/' + settings['data'][9]['value'],
				bigLogo: Env.get('IMAGE_HOST') + '/' + settings['data'][10]['value'],
				smallLogo: Env.get('IMAGE_HOST') + '/' + settings['data'][11]['value']
			}
			
			let template = view.render('reset', {
				meta: meta,
				paramsUrl: pr,
				data: {
					user: user,
					param: reqparam
				}
			})
			
			return await minifyHTML.minify(template)
		} else {
			return response.redirect('/home/' + pr)
		}
	}
	
	async postReset({ request, response, view, session }) {
		const { id, key, password, confirm_password, pr } = request.only(['id', 'key', 'password', 'confirm_password', 'pr'])
		
		session.forget('principal')
		await session.put('principal', pr)

		const reqparam = {
			id: id,
			key: key,
			password: password,
			confirm_password: confirm_password,
			principal: pr
		}

		const rules = {
			id: 'required',
			key: 'required',
			password: 'required',
			confirm_password: 'required'
		}
		
		const validation = await validateAll(reqparam, rules)
		
		if (validation.fails()) {
			session.withErrors(validation.messages()).flashExcept(['password', 'confirm_password'])
			return response.redirect('back')
		} else {
			if (password == confirm_password) {
				let reset = await ApiService.getData(Env.get('API_HOST') + '/reset', 'POST', reqparam)
				
				if (reset['code'] == '2000') {
					session.flash({ notification: 'Password recovered, please login with new password!' })
					return response.redirect('/login')
				} else {
					session.flash({ notification: 'Check session to reset has expired, please re-forgot password!' })
					return response.redirect('back')
				}
			} else {
				session.flash({ notification: 'Password is not match!' })
				return response.redirect('back')
			}
		}
	}

	async logout({ request, response, view, session }) {
		let logout = await ApiService.getData(Env.get('API_HOST') + '/logout')
		const { code } = request.only(['code'])
		
		session.forget('user')
		session.forget('principal')
		response.clearCookie('aino-rememberme')
		let pageIp
		let getPageIp = request.cookie('aino-session')
		if (getPageIp) {
			pageIp = getPageIp
		} else {
			pageIp = request.ip().replace(/[.]/g, '')
		}
		await Cache.tags(pageIp).flush()
		await delay(1000)
		return response.redirect('/home/'+code)
	}

	async checkOAuth(ally, driver) {
		try {
			return await ally.driver(driver).getUser()
		} catch(error) {
			return false
		}
	}


	async otpPages({ request, response, auth, view, session }) {
		let user = session.get('user')
		let principal = session.get('principal')
		let email = session.get('email')

		if (user) {
			return response.redirect('/home/'+principal)
		} else {
			let settings = await ApiService.getData(Env.get('API_HOST') + '/getsetting')

			let meta = {
				title: 'Confirm OTP',
				description: 'Confirm OTP - ' + settings['data'][1]['value'],
				keywords: settings['data'][2]['value'],
				copyright: settings['data'][3]['value'],
				author: settings['data'][4]['value'],
				url: Env.get('BASE_URL'),
				image: Env.get('BASE_URL') + '/' + settings['data'][5]['value'],
				shortcutIcon: Env.get('IMAGE_HOST') + '/' + settings['data'][9]['value'],
				bigLogo: Env.get('IMAGE_HOST') + '/' + settings['data'][10]['value'],
				smallLogo: Env.get('IMAGE_HOST') + '/' + settings['data'][11]['value']
			}
			
			let template = view.render('confirm_otp', {
				meta: meta,
				data: {
					email: email
				}
			})
			
			return await minifyHTML.minify(template)
		}
	}

	async postOtp({ request, response, view, session }) {
		const { otp } = request.only(['otp'])
		let principal = session.get('principal')

		const reqparam = {
			otp: otp,
			principal: principal
		}
		
		const rules = {
			otp: 'required',
		}
		
		const validation = await validateAll(reqparam, rules)
		
		if (validation.fails()) {
			session.withErrors(validation.messages()).flashAll()
			return response.redirect('back')
		} else {
				// let reset = await ApiService.getData(Env.get('API_HOST') + '/reset', 'POST', reqparam)
				
			// if (reset['code'] == '2000') {
				session.flash({ notification: 'Verify OTP Success!' })
				return 1
			// } else {
			// 	session.flash({ notification: 'Check session to reset has expired, please re-forgot password!' })
			// 	return response.redirect('back')
			// }
		}
	}

	async postResendOtp({ request, response, view, session }) {
		const { email } = request.only(['email'])
		
		let principal = session.get('principal')

		const reqparam = {
			email: email,
			principal: principal,
			base_url: Env.get('BASE_URL')
		}

		const rules = {
			email: 'required',
			principal: 'required',
			base_url: 'required'
		}
		
		const validation = await validateAll(reqparam, rules)
		
		if (validation.fails()) {
			session.withErrors(validation.messages())
			return response.redirect('back')
		} else {
			let resend = await ApiService.getData(Env.get('API_HOST') + '/resendotp', 'POST', reqparam)
			if(resend['code'] == '2000') {
				session.flash({ notification: 'Resend Otp Success!' })
				return 1
			} else {
				session.flash({ notification: 'Oh, Snap! Something happen!' })
				return 0
			}
		}
	}
}

module.exports = AccountController
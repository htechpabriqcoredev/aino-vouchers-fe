const { hooks } = require('@adonisjs/ignitor')

hooks.before.providersBooted(() => {
	const View = use('View')
	const Logger = use('Logger')
	const moment = require('moment')
	const Server = use('Adonis/Src/Server')
	const Env = use('Env')
	
	Server.getInstance().timeout = 300000
	
	View.global('hasRole', function (users_roles, role) {
		return ((users_roles) && (users_roles.indexOf(role) >= 0))
	})
	
	View.global('yearNow', function () {
		let d = new Date()
		let n = d.getFullYear()
		return n
	})

	View.global('formatDate', function (date, format = null) {
		const format_date = moment(new Date(date))
		if(format) {
			return format_date.format(format)	
		}
		return format_date.format('MM/DD/YYYY')
	})

	View.global('formatRupiah', function (currency) {
		let x = parseInt(currency);
		return 'Rp. '+ x.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")
	})
	
	View.global('loginImage', function () {
		return Math.floor(Math.random() * (5 - 1 + 1) ) + 1
	})
	
	View.global('loginBackground', function () {
		let key =  Math.floor(Math.random() * (4 - 0 + 1) ) + 0
		let color = ["#A06DF2", "#FAFAFA", "#5CBAFF", "#44C97D", "#152733"]
		return color[key]
	})

	View.global('baseURL', function () {
		return Env.get('BASE_URL') 
	})
})